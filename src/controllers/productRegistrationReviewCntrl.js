import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import {AddAccountContactReq} from 'account-contact-service-sdk';
import AssetServiceSdk from 'asset-service-sdk';
import {UpdateAccountIdOfAssetsReq} from 'asset-service-sdk';
import {AddCommercialAccountReq} from 'account-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

import PromptOnBackToReview  from './popups/submittedWarning.html!text';

angular
    .module('productRegistrationReviewCntrl',['ui.bootstrap']);

export default class productRegistrationReviewCntrl{

    constructor(
        $scope,
        $q,
        $uibModal,
        $location,
        sessionManager, 
        identityServiceSdk,
        accountServiceSdk,
        accountContactServiceSdk,
        customerSourceServiceSdk,
        managementCompanyServiceSdk,
        buyingGroupServiceSdk,
        partnerRepAssociationServiceSdk,
        partnerRepServiceSdk,
        assetServiceSdk,
        partnerSaleRegistrationServiceSdk) {

        var getReview=JSON.parse(window.localStorage.getItem("reviewList"));
        
        if(window.localStorage.getItem("isFromSubmitPage") == "true"){
            $scope.productsSubmitList = JSON.parse(window.localStorage.getItem("SubmittedDraft"));
            
            $scope.modalInstance = $uibModal.open({
                animation : true,
                scope : $scope,
                template : PromptOnBackToReview,
                size : 'sm',
                backdrop : 'static'
            });

            $scope.handleOnOk = function(){
                $scope.modalInstance.dismiss('cancel');
            
                window.localStorage.clear();
            }
        } else {

            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken =>
                        resolve(accessToken)
                    )
            )
            .then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                    partnerSaleRegistrationServiceSdk
                        .getPartnerSaleRegistrationDraftWithDraftId(getReview, accessToken)
                        .then(response =>
                            resolve(response)
                        )
                )
                .then(PartnerCommercialSaleRegSynopsisView => {
                    $scope.loader = false;
                    $scope.productsReviewList = PartnerCommercialSaleRegSynopsisView;
                    $scope.getFacilityReviewData($scope.productsReviewList.facilityId);
                });
            });
        }
        
        $scope.getFacilityReviewData = function(id){

            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken =>
                        resolve(accessToken)
                    )
            ).then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                    accountServiceSdk
                        .getCommercialAccountWithId(id, accessToken)
                        .then(response =>
                            resolve(response)
                        ).catch(function(e){
                            console.log("error in facilityInfo......",e);
                            $("#err").show().fadeOut(8000);
                            $scope.loader = false;
                            $scope.errorMessage = e.response;
                            })
                )
                .then(facilityInfo => {
                    $scope.loader = false;
                    $scope.facilityInfo = JSON.parse(JSON.stringify(facilityInfo));
                    if($scope.facilityInfo.customerBrand==null){
                        $scope.facilityInfo.customerBrand="No match";
                    }else{
                        $scope.facilityInfo.customerBrand=$scope.facilityInfo.customerBrand;
                    }
                    if($scope.facilityInfo.customerSubBrand == null){
                        $scope.facilityInfo.customerSubBrand = "No match";
                    }else{
                        $scope.facilityInfo.customerSubBrand = $scope.facilityInfo.customerSubBrand;
                    }
                        window.localStorage.setItem("SubmittedFacilityInfo",JSON.stringify($scope.facilityInfo));
                })
                .catch(function(e){
                    console.log("Error in ContactInfo......",e);
                    $("#err").show().fadeOut(8000);
                    $scope.loader = false;
                    $scope.errorMessage = e.response;
                });
                $scope.getFacilityContactInfo(accessToken);
                $scope.getSaleInfo();
            });

        };
        
        $scope.getFacilityContactInfo = function(accessToken){
            $scope.loader = true;

            $q(resolve =>
                accountContactServiceSdk
                    .getAccountContactWithId($scope.productsReviewList.facilityContactId, accessToken)
                    .then(response =>
                        resolve(response)
                    )
                    .catch(function(e){
                        console.log("error in facilityContacts......",e);
                        $("#err").show().fadeOut(8000);
                        $scope.loader = false;
                        $scope.errorMessage = e.response;
                })
            ).then(facilityContacts => {
                $scope.loader = false;
                $scope.facilityContacts = facilityContacts;
                window.localStorage.setItem("FacilityContactInfo",JSON.stringify($scope.facilityContacts));
            });
        };
        
        $scope.getSaleInfo = function(){
            $scope.reviewDraftID = JSON.parse(window.localStorage.getItem("draftId"));

            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken =>
                        resolve(accessToken)
                    )
            )
            .then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                    partnerSaleRegistrationServiceSdk
                        .getPartnerSaleRegistrationDraftWithDraftId($scope.reviewDraftID, accessToken)
                        .then(response =>
                            resolve(response)
                        )
                )
                .then(PartnerCommercialSaleRegSynopsisView => {
                    $scope.loader = false;

                    $scope.reviewDraft = PartnerCommercialSaleRegSynopsisView;
                    $scope.getCustomerSourceById($scope.reviewDraft.customerSourceId);
                    $scope.getManagementCompanyById($scope.reviewDraft.managementCompanyId);
                    $scope.getBuyingGroupById($scope.reviewDraft.buyingGroupId);
                    $scope.getDealerById($scope.reviewDraft.partnerRepUserId);
                });
            });
        };
        
        $scope.getCustomerSourceById = function(id){            
            //populate customer source dropdown
            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken =>
                        resolve(accessToken)
                    )
            )
            .then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                    customerSourceServiceSdk
                        .listCustomerSources(accessToken)
                        .then(response =>
                            resolve(response)
                        )
                )
                .then(customerSources => {
                    $scope.loader = false;

                    $scope.customerSources = customerSources;
                    angular.forEach($scope.customerSources,function( val , key ){
                        if(val.id == id){
                            $scope.reviewDraft.customerSource = val._name;
                            window.localStorage.setItem("FacilityProductInfo",JSON.stringify($scope.reviewDraft));
                        }
                    });
                });
            });
        };
        
        $scope.getManagementCompanyById = function(id){

            $q(resolve =>
               sessionManager
                   .getAccessToken()
                   .then(accessToken =>
                       resolve(accessToken)
                   )

            )
            .then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                    managementCompanyServiceSdk
                        .listManagementCompanies(accessToken)
                        .then(
                            response =>
                                resolve(response)
                        )
                )
                .then(managementCompanies => {
                    $scope.loader = false;

                    $scope.managementCompanies = JSON.parse(managementCompanies.response);
                    angular.forEach($scope.managementCompanies,function( val , key ){
                        if(val.id == id){
                            $scope.reviewDraft.managementCompany = val.name;
                            window.localStorage.setItem("FacilityProductInfo",JSON.stringify($scope.reviewDraft));
                        }
                    });
                });
            });
        };
        
        $scope.getBuyingGroupById = function(id){

            $q(resolve =>
               sessionManager
                   .getAccessToken()
                   .then(accessToken =>
                       resolve(accessToken)
                   )
            )
            .then(accessToken => {
                $scope.loader = true;

                $q(resolve =>
                   buyingGroupServiceSdk
                       .listBuyingGroups(accessToken)
                       .then(response =>
                           resolve(response)
                       )
                )
                .then(buyingGroups => {
                    $scope.loader = false;

                    $scope.buyingGroups = JSON.parse(buyingGroups.response);
                    angular.forEach($scope.buyingGroups,function( val , key ){
                        if(val.id == id){
                            $scope.reviewDraft.buyingGroup = val.name;
                            window.localStorage.setItem("FacilityProductInfo",JSON.stringify($scope.reviewDraft));
                        }
                    });
                });
            });
        };
        
        $scope.getDealerById = function(id){
            $scope.loader = true;

            /*$q(resolve =>
                identityServiceSdk
                    .getUserInfo(accessToken)
                    .then(userInfo =>
                        resolve(userInfo)
                    )
            ).then(userInfo => {
                    $scope.loader = true;

                $q(resolve =>
                    partnerRepAssociationServiceSdk
                        .listPartnerRepAssociationsWithPartnerId(userInfo._account_id, accessToken)
                        .then(partnerRepAssociations =>
                            resolve(partnerRepAssociations)
                        )
                ).then(partnerRepAssociations => {


                    var partnerRepIds = Array.from(partnerRepAssociations, (partnerRepAssociation) => {
                            return partnerRepAssociation.repId;
                        }
                    );

                    $q(resolve =>
                        partnerRepServiceSdk
                            .getPartnerRepsWithIds(partnerRepIds, accessToken)
                            .then(partnerReps =>
                                resolve(partnerReps)
                            )
                    ).then(partnerReps => {
                        $scope.loader = false;

                        $scope.dealers = partnerReps;
                        angular.forEach($scope.dealers,function( val , key ){
                            if(val.id == id){
                                $scope.reviewDraft.dealerRep = val._firstName +" "+ val._lastName;
                            }
                        });
                        window.localStorage.setItem("FacilityProductInfo",JSON.stringify($scope.reviewDraft));
                    });
                });
            });*/

            if(id) {
                $q(resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
                ).then(accessToken =>
                    $q(resolve =>
                        partnerRepServiceSdk
                            .getPartnerRepWithId(id, accessToken)
                            .then(partnerRep => {
                                    $scope.reviewDraft.dealerRep = partnerRep.firstName + " " + partnerRep.lastName;
                                    $scope.loader = false;
                                    resolve($scope.reviewDraft);
                                }
                            )
                    ).then( reviewDraft => {
                        window.localStorage.setItem("FacilityProductInfo",JSON.stringify(reviewDraft));
                    })
                );
            } else {
                $scope.loader = false;
                window.localStorage.setItem("FacilityProductInfo",JSON.stringify($scope.reviewDraft));
            }
        };
        
        var draftId = window.localStorage.getItem("draftId");
        $scope.productRegistrationReview_Next = function(){

            $q(resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken =>
                        resolve(accessToken)
                        )
            ).then(accessToken=>{
                $scope.loader = true;

                $q(resolve =>
                    identityServiceSdk
                        .getUserInfo(accessToken)
                        .then(userInfo =>
                            resolve(userInfo)
                        )
                ).then(userInfo => {
                    
                    var submitRequest = {}, partnerRepId;
                    
                    submitRequest.id = draftId;
                    
                    var given_name = userInfo.given_name ? userInfo.given_name : "";
                    var family_name = userInfo.family_name ? userInfo.family_name : "";
                    
                    submitRequest.submittedByName = given_name +" "+ family_name;
                    
                    partnerRepId = window.localStorage.getItem("partnerRepId");
                    
                    if(partnerRepId){                    
                        submitRequest.partnerRepId = partnerRepId;
                        
                        var dealerRepDetails = JSON.parse(localStorage.getItem("dealerRepDetails"));
                        
                        submitRequest.firstName = dealerRepDetails.firstName;
                        submitRequest.lastName = dealerRepDetails.lastName;
                        submitRequest.emailAddress = dealerRepDetails.emailAddress;
                    }

                    $q(resolve =>
                        partnerSaleRegistrationServiceSdk
                            .submitPartnerCommercialSaleRegDraft(submitRequest, accessToken)
                            .then(response =>
                                resolve(response)
                            )
                    ).then(PartnerCommercialSaleRegSynopsisView => {

                        window.localStorage.setItem("SubmittedDraft", JSON.stringify(PartnerCommercialSaleRegSynopsisView));

                        $q(resolve =>
                           assetServiceSdk
                            .updateAccountIdOfAssets(
                                new UpdateAccountIdOfAssetsReq(
                                    PartnerCommercialSaleRegSynopsisView.facilityId,
                                    JSON.parse(window.localStorage.getItem("assetIds"))
                                    ),
                                    accessToken
                            )
                            .then(response =>
                                resolve(response)
                            )
                        ).then((response) => {
                            $scope.loader = false;
                            window.localStorage.setItem("isFromSubmitPage","true");
                            $scope.modalInstance = $uibModal.open({
                                scope: $scope,
                                template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                '<div class="modal-body">Registration submitted successfully </div>' +
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="review_confirm()">Ok</button></div>',
                                size: 'sm',
                                backdrop: 'static'
                            });
                        });
                    });
                });
            });

        };
        $scope.review_confirm=function(){
            $location.path("/productRegistrationSubmit");
        };
        $scope.productRegistrationReview_Previous=function(){
            window.localStorage.setItem("isFromProdRegPage","false");
            $location.path("/productRegistrationPricing");
        };
        $scope.Back_Drafts=function(){
            $location.path("/");
        };
    }
};

productRegistrationReviewCntrl
    .$inject=[
        '$scope',
        '$q',
        '$uibModal',
        '$location',
        'sessionManager',
        'identityServiceSdk',
        'accountServiceSdk',
        'accountContactServiceSdk',
        'customerSourceServiceSdk',
        'managementCompanyServiceSdk',
        'buyingGroupServiceSdk',
        'partnerRepAssociationServiceSdk',
        'partnerRepServiceSdk',
        'assetServiceSdk',
        'partnerSaleRegistrationServiceSdk'
    ];