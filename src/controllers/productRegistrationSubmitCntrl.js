import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';

angular
    .module('productRegistrationSubmitCntrl',['ui.bootstrap']);
export default class productRegistrationReviewCntrl{
    constructor($scope, $uibModal,$location) {
        var submitDraft=JSON.parse(window.localStorage.getItem("SubmittedDraft"));
        window.localStorage.setItem("isFromSubmitPage","true");

        $scope.productsSubmitList=submitDraft;

        $scope.productRegistrationHome=function(){
            $location.path("/productRegistration");
        };
        $scope.Back_Drafts=function(){
            $location.path("/");
        };

        function convertUTCDateToLocalDate(date) {
            var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);
            var offset = date.getTimezoneOffset() / 60;
            var minutes = date.getMinutes() / 60;
            var hours = date.getHours();
            newDate.setHours(hours - offset + minutes);
            return newDate;
        }
        var date = convertUTCDateToLocalDate(new Date($scope.productsSubmitList.draftSubmittedDate));
        $scope.productsSubmitList.draftSubmittedDate=date.toLocaleString();
        
        $scope.facilityInfo = JSON.parse(window.localStorage.getItem("SubmittedFacilityInfo"));
        $scope.facilityContacts = JSON.parse(window.localStorage.getItem("FacilityContactInfo"));
        $scope.productInfo = JSON.parse(window.localStorage.getItem("FacilityProductInfo"));		
	
		$scope.setExtendedWarrantyAssets = function(){
			window.localStorage.setItem("navigatedFrom" , "commercialRegistration");
			window.localStorage.setItem("extendedWarrantyAssets" , window.localStorage.getItem("SubmittedDraft"));
		};
    }
};

productRegistrationReviewCntrl.$inject=[
    '$scope',
    '$uibModal',
    '$location'
];