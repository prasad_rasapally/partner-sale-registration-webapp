/**
 * @class {prepareSpiffEligibleCheckRequestFactory}
 */

export default class prepareSpiffEligibleCheckRequestFactory {

    constructor(){
        
    }
    prepareSpiffEligibleRequest( assetsList ){
        var request = {};
        
        request.simpleLineItems = [];
        
        request.compositeLineItems = [];        
        
        if( assetsList.simpleLineItems.length ){
            assetsList.simpleLineItems.forEach(function( val , ind ) {
                request.simpleLineItems.push( {"serialNumber" : val.serialNumber.substr(0,4)} );
            });
        }
        
        if( assetsList.compositeLineItems.length ){
            request.compositeLineItems.push( {"components" : []} );
            
            assetsList.compositeLineItems.forEach(function( val , index ) {                
                request.compositeLineItems[0].components
                    .push( {"serialNumber" : val.components[index].serialNumber.substr(0,4)});
            });
        }
        
        return request;
    };
    
    prepareSpiffEligibleRequestWithMultipleAssets( assetsList ){
        var request = {};
        
        request.simpleLineItems = [];
        
        request.compositeLineItems = [];
        
        if( assetsList.simpleLineItems.length ){
            assetsList.simpleLineItems.forEach(function( val , ind ) {
                request.simpleLineItems.push( {"serialNumber" : val.serialNumber.substr(0,4)} );
            });
        }
        
        if( assetsList.compositeLineItems.length ){
            assetsList.compositeLineItems.forEach(function( component , index ) {
                request.compositeLineItems.push( {"components" : []} );
                component.components.forEach(function( val , ind ) {
                    request.compositeLineItems[index].components.push( {"serialNumber" : val.serialNumber.substr(0,4)} );
                });
            });
        }
        
        return request;
    }
    
    static prepareSpiffEligibleCheckRequest(){
        return new prepareSpiffEligibleCheckRequestFactory();
    }
}

prepareSpiffEligibleCheckRequestFactory.prepareSpiffEligibleCheckRequest.$inject = [];