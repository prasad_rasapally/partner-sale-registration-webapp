import PrepareSpiffEligibleCheckRequestFactory from './prepareSpiffEligibleCheckRequestFactory';
import MergeSpiffDataWithAssetsFactory from './mergeSpiffDataWithAssetsFactory';
import MergeEWDataWithAssetsFactory from './mergeEWDataWithAssetsFactory';

angular
    .module("productRegistrationWebApp.factories",[])
    .factory(
        "prepareSpiffEligibleCheckRequestFactory", 
         PrepareSpiffEligibleCheckRequestFactory.prepareSpiffEligibleCheckRequest
    )
    .factory(
        "mergeSpiffDataWithAssetsFactory", 
         MergeSpiffDataWithAssetsFactory.mergeSpiffDataWithAssetsFactory
    )
    .factory(
        "mergeEWDataWithAssetsFactory", 
         MergeEWDataWithAssetsFactory.mergeEWDataWithAssetsFactory
    );


