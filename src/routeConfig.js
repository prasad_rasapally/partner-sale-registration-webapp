import productRegistrationDraftTemplate from './templates/productRegistrationDrafts.html!text';
import productRegistrationDraftCntrl from './controllers/productRegistrationDraftCntrl';
import productRegistrationTemplate from './templates/productRegistration.html!text';
import productRegistrationCntrl from './controllers/productRegistrationCntrl';
import productRegistrationPricingTemplate from './templates/productRegistrationPricing.html!text';
import productRegistrationPricingCntrl from './controllers/productRegistrationPricingCntrl';
import productRegistrationReviewTemplate  from './templates/productRegistrationReview.html!text';
import productRegistrationReviewCntrl from './controllers/productRegistrationReviewCntrl';
import productRegistrationSubmitTemplate from './templates/productRegistrationSubmit.html!text';
import productRegistrationSubmitCntrl from './controllers/productRegistrationSubmitCntrl';
import productRegistrationEditDraftCntrl from './controllers/productRegistrationEditDraftCntrl';
import productRegistrationViewDraftCntrl from './controllers/productRegistrationViewDraftCntrl';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when('/',
                {
                    template:productRegistrationDraftTemplate,
                    controller: productRegistrationDraftCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistration',
                {
                    template:productRegistrationTemplate,
                    controller: productRegistrationCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistrationDraftEdit',
                {
                    template:productRegistrationTemplate,
                    controller: productRegistrationEditDraftCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistrationDraftView',
                {
                    template:productRegistrationTemplate,
                    controller: productRegistrationViewDraftCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistrationPricing',
                {
                    template:productRegistrationPricingTemplate,
                    controller: productRegistrationPricingCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistrationReview',
                {
                    template:productRegistrationReviewTemplate,
                    controller: productRegistrationReviewCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/productRegistrationSubmit',
                {
                    template:productRegistrationSubmitTemplate,
                    controller: productRegistrationSubmitCntrl,
                    controllerAs:'controller'
                }
            )
            .otherwise({
                redirectTo:"/"
            });

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];