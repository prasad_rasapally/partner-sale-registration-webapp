import AccountPermissionsServiceSdk from 'account-permissions-service-sdk';

/**
 * @class {AccountPermissionService}
 */

export default class AccountPermissionService {
    _$q;

    _accountPermissionServiceSdk : AccountPermissionServiceSdk;

    constructor(
        $q,

        accountPermissionServiceSdk : AccountPermissionServiceSdk

    ){
        /**
         initialization
         */
        if(!$q){
            throw new TypeError('$q required');
        }
        this._$q = $q;
        
        if(!accountPermissionServiceSdk){
            throw new TypeError('accountPermissionServiceSdk required');
        }
        this._accountPermissionServiceSdk = accountPermissionServiceSdk;
    }
    /**
     methods
     */
    getAccountPermissionsWithId( partnerAccountId , accessToken ){
        return this._$q((resolve , reject) =>
            this._accountPermissionServiceSdk
                .getAccountPermissionsWithId( partnerAccountId , accessToken )
                .then(
                    accountPermissionsSynopsisView => resolve( accountPermissionsSynopsisView )
                ).catch(function(error){
                    reject('Unable to load accountPermissionsSynopsisView.');
                })
        )
    };
}

AccountPermissionService.$inject = [
    '$q',
    'accountPermissionsServiceSdk'
];