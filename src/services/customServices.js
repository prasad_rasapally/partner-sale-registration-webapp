import AccountPermissionService from './accountPermissionService';

angular
    .module("productRegistrationWebApp.services",[])
    .service("accountPermissionService", AccountPermissionService);


